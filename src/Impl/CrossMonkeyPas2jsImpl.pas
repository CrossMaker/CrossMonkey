unit CrossMonkeyPas2jsImpl;

{$MODE Delphi}

interface

uses
  JS,
  Classes,
  SysUtils,
  StrUtils,
  Web,
  CrossMonkey;

type

  // TCrossElement
  //
  TCrossElement = class(TInterfacedObject, ICrossElement)
  private
    FElement : TJSElement;
  public
    constructor Create(AElement : TJSElement);
    destructor Destroy(); override;
    function getElementById(AId : string) : ICrossElement;
    function childrenCount : Integer;
    function getChildren(AIndex : Integer) : ICrossElement;
    procedure setAttribute(AName, AValue : string);
    function getAttribute(AName : string) : string;
    procedure setStylePropertie(AName, AValue : string);
    function getStylePropertie(AName : string) : string;
    function getInnertText : string;
    procedure setInnerText(AInnerText : string);
    function getInnerHTML : string;
    procedure setInnerHTML(AInnerText : string);
    procedure focus;
    procedure addEventListener(AEvent : string; AProcedure : TProcEvent; AUseCapture : boolean);
    procedure RemoveEventListener(AEvent : string; AProcedure : TProcEvent);

    function getValue : string;

    procedure setValue(AValue : string);
  end;


  // TCrossForm
  //
  TCrossForm = class(TInterfacedObject, ICrossForm)
  private
      FWindow : TJSWindow;
  public
    constructor Create;
    destructor Destroy(); override;
    procedure LoadHTMLFromFile(const FileName: string; AOnloadEvent : TProcEvent);
    procedure LoadHTMLFromURL(const URL: string; AOnloadEvent : TProcEvent);
    function getDOM : ICrossElement;
  end;


  // ICrossApplication
  //
  TCrossApplication= class(TInterfacedObject, ICrossApplication)
  private
  public
    {:@desc ICrossApplication }
    constructor Create;
    destructor Destroy(); override;
    procedure Run;
    function CreateForm : ICrossForm;
    procedure Terminate;
  end;



implementation


{ TCrossElement }

constructor TCrossElement.Create(AElement : TJSElement);
begin
  inherited;
  FElement := AElement;
end;

destructor TCrossElement.Destroy();
begin

end;

function TCrossElement.getElementById(AId : string) : ICrossElement;
var
  LElement : TJSElement;
begin
  LElement := document.GetElementbyId(AId);
  if LElement <> nil then
    Result := TCrossElement.Create(LElement)
  else
    Result := nil;
end;

function TCrossElement.childrenCount : Integer;
begin
  Result := FElement.childElementCount;
end;

function TCrossElement.getChildren(AIndex : Integer) : ICrossElement;
var
  LElement : TJSElement;
begin
  LElement := FElement.children[AIndex] as TJSElement;
  if LElement <> nil then
    Result := TCrossElement.Create(LElement)
  else
    Result := nil;
end;


procedure TCrossElement.setAttribute(AName, AValue : string);
begin
  FElement.setAttribute(AName, AValue);
end;

function TCrossElement.getAttribute(AName : string) : string;
begin
  Result := FElement.getAttribute(aName);
end;

procedure TCrossElement.setStylePropertie(AName, AValue : string);
begin
  //to do
end;

function TCrossElement.getStylePropertie(AName : string) : string;
begin
  //to do
end;

function TCrossElement.getInnertText : string;
begin
  Result := FElement.InnerText;
end;

procedure TCrossElement.setInnerText(AInnerText : string);
begin
  FElement.InnerText := AInnerText;
end;

function TCrossElement.getInnerHTML : string;
begin
  Result := FElement.InnerHTML;
end;

procedure TCrossElement.setInnerHTML(AInnerText : string);
begin
  FElement.InnerHTML := AInnerText;
end;

procedure TCrossElement.focus;
begin
//  FElement.focus();
end;

procedure TCrossElement.addEventListener(AEvent : string; AProcedure : TProcEvent; AUseCapture : boolean);
begin
  FElement.addEventListener(AEvent, procedure begin AProcedure() end);
end;

procedure TCrossElement.RemoveEventListener(AEvent : string; AProcedure : TProcEvent);
begin
  FElement.RemoveEventListener(AEvent, procedure begin AProcedure() end);
end;

function TCrossElement.getValue : string;
begin
  if (FElement is TJSHTMLInputElement) then
    Result := (FElement as TJSHTMLInputElement).Value;
end;

procedure TCrossElement.setValue(AValue : string);
begin
  if (FElement is TJSHTMLInputElement) then
    (FElement as TJSHTMLInputElement).Value := AValue;
end;

{ TCrossApplication }

constructor TCrossApplication.Create;
begin

end;

destructor TCrossApplication.Destroy();
begin

end;

procedure TCrossApplication.Run;
begin

end;

function TCrossApplication.CreateForm : ICrossForm;
begin
  Result := TCrossForm.Create;
end;

procedure TCrossApplication.Terminate;
begin

end;

{ TCrossForm }

constructor TCrossForm.Create;
begin
  FWindow := window;
end;

destructor TCrossForm.Destroy();
begin
  FWindow.close;
end;


procedure TCrossForm.LoadHTMLFromFile(const FileName: string; AOnloadEvent : TProcEvent);
begin
  asm
  fetch(FileName)
  .then(function(response) {
        // When the page is loaded convert it to text
        return response.text()
    })
  .then(function(html) {
      document.documentElement.innerHTML = html;
      AOnloadEvent();
     })
  .catch(function(err) {
        console.log('Failed to fetch page: ', err);
    });
  end;
end;

procedure TCrossForm.LoadHTMLFromURL(const URL: string; AOnloadEvent : TProcEvent);
begin
  LoadHTMLFromFile(URL, AOnloadEvent);
end;

function TCrossForm.getDOM : ICrossElement;
begin
  Result := TCrossElement.Create(TJSElement(document.documentElement));
end;

end.


