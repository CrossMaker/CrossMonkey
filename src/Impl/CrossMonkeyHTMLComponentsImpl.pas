unit CrossMonkeyHTMLComponentsImpl;

{$IFDEF FPC}
  {$MODE Delphi}
{$ENDIF}
interface

uses
  classes,
  Generics.Collections,
{$IFDEF FPC}
  Interfaces,
  Forms,
{$ENDIF}
{$IFDEF DCC}
  FMX.Forms,
{$ENDIF}

  htutils,
  hthints,
  htcanvas,
  {$IFDEF FPC}
  htmlcomp,
  htmldraw,
  htsvg,
  htcanvaslazarus,
  htdefscriptadapter,
  {$IFDEF MSWINDOWS}
  htcanvasdx,
  {$ENDIF}
  {$ENDIF}
  {$IFDEF DCC}
  fmx.fhtmlcomp,
  fmx.htcanvasfmx,
  fmx.fhtmldraw,
  fmx.fhtsvg,
  {$ENDIF}
  CrossMonkey;

type

  // TCrossElement
  //
  TCrossElement = class(TInterfacedObject, ICrossElement)
  private
    FElement : TElement;
  public
    constructor Create(AElement : TElement);reintroduce;
    destructor Destroy(); override;
    function getElementById(AId : string) : ICrossElement;
    function childrenCount : Integer;
    function getChildren(AIndex : Integer) : ICrossElement;
    procedure setAttribute(AName, AValue : string);
    function getAttribute(AName : string) : string;
    procedure setStylePropertie(AName, AValue : string);
    function getStylePropertie(AName : string) : string;
    function getInnertText : string;
    procedure setInnerText(AInnerText : string);
    function getInnerHTML : string;
    procedure setInnerHTML(AInnerText : string);
    procedure focus;
    procedure addEventListener(AEvent : string; AProcedure : TProcEvent; AUseCapture : boolean);
    procedure RemoveEventListener(AEvent : string; AProcedure : TProcEvent);
    function getValue : string;
    procedure setValue(AValue : string);
  end;


  // THtCrossadapter
  //
  THtCrossadapter = class(THtScriptAdapter)
  private
  public
    procedure Run(Element: TElement; const Event: string; const Script: hstring); override;
  end;

  // TCrossForm
  //
  TCrossForm = class(TInterfacedObject, ICrossForm)
  private
    FMainform : TForm;
    Fhtpanel : THtpanel;
  public
    constructor Create;
    destructor Destroy(); override;
    procedure LoadHTMLFromFile(const FileName: string; AOnloadEvent : TProcEvent);
    procedure LoadHTMLFromURL(const URL: string; AOnloadEvent : TProcEvent);
    function getDOM : ICrossElement;
  end;


  // ICrossApplication
  //
  TCrossApplication= class(TInterfacedObject, ICrossApplication)
  private
  public
    {:@desc ICrossApplication }
    constructor Create;
    destructor Destroy(); override;
    procedure Run;
    function CreateForm : ICrossForm;
    procedure Terminate;
  end;

var
   gRegisteredEvents : TDictionary<string, TProcEvent>;

implementation

uses

{$IFDEF FPC}
  Controls;
{$ENDIF}
{$IFDEF DCC}
  FMX.Types,
  FMX.Controls;
{$ENDIF}


{ TCrossElement }

constructor TCrossElement.Create(AElement : TElement);
begin
  FElement := AElement;
end;

destructor TCrossElement.Destroy();
begin

end;

function TCrossElement.getElementById(AId : string) : ICrossElement;
var
  LElement : TElement;
begin
  LElement := FElement.Document.GetElementbyId(AId);
  if LElement <> nil then
    Result := TCrossElement.Create(LElement)
  else
    Result := nil;
end;


function TCrossElement.childrenCount : Integer;
begin
  Result := FElement.ChildNodeList.Count;
end;

function TCrossElement.getChildren(AIndex : Integer) : ICrossElement;
var
  LElement : TElement;
begin
  LElement := FElement.Document.ChildNodes[AIndex] as TElement;
  if LElement <> nil then
    Result := TCrossElement.Create(LElement)
  else
    Result := nil;
end;

procedure TCrossElement.setAttribute(AName, AValue : string);
begin
  FElement.SetAttribute(AName, AValue);
end;

function TCrossElement.getAttribute(AName : string) : string;
begin
  Result := FElement.GetAttribute(AName);
end;

procedure TCrossElement.setStylePropertie(AName, AValue : string);
begin;
  //to do
end;

procedure TCrossElement.setValue(AValue: string);
begin
  if (FElement is TBaseInputElement) then
    (FElement as TBaseInputElement).Value := AValue;
end;



function TCrossElement.getStylePropertie(AName : string) : string;
begin
  //to do
end;

function TCrossElement.getValue: string;
begin
  if (FElement is TBaseInputElement) then
    Result := (FElement as TBaseInputElement).Value;
end;

function TCrossElement.getInnertText : string;
begin
  Result := FElement.InnerText;
end;

procedure TCrossElement.setInnerText(AInnerText : string);
begin
  FElement.InnerText := AInnerText;
end;

function TCrossElement.getInnerHTML : string;
begin
  Result := FElement.InnerHTML;
end;

procedure TCrossElement.setInnerHTML(AInnerText : string);
begin
  FElement.InnerHTML := AInnerText;
end;

procedure TCrossElement.focus;
begin
  FElement.Focus;
end;

procedure TCrossElement.addEventListener(AEvent: string; AProcedure: TProcEvent;
                                            AUseCapture: boolean);
begin
  FElement.Attributes.Add('on'+AEvent, ' ');

  gRegisteredEvents.Add(FElement.Id +AEvent, AProcedure);
end;

procedure TCrossElement.RemoveEventListener(AEvent: string; AProcedure: TProcEvent);
begin
  FElement.Attributes.ClearAttr('on'+AEvent);

  if gRegisteredEvents.ContainsKey(FElement.Id + AEvent) then
    gRegisteredEvents.Remove(FElement.Id + AEvent);
end;

{ TCrossApplication# }

constructor TCrossApplication.Create;
begin
  {$IFDEF FPC}
  HtDefaultCanvasClass := THtCanvasLazarus;
  {$ENDIF}
  {$IFDEF MSWINDOWS}
  //HtDefaultCanvasClass := THtCanvasDX;
  {$ENDIF}
  {$IFDEF DCC}
  HtDefaultCanvasClass := THtCanvasFMX;
  {$ENDIF}
  Application.initialize;
end;

{ TCrossApplication }

destructor TCrossApplication.Destroy();
begin
  inherited;
end;

procedure TCrossApplication.Run;
begin
  Application.Run;
end;

function TCrossApplication.CreateForm : ICrossForm;
begin
  Result := TCrossForm.Create;
end;

procedure TCrossApplication.Terminate;
begin
  Application.Terminate();
end;

{ TCrossForm }


constructor TCrossForm.Create;
begin
  FMainform := TForm.CreateNew(nil);
  Fhtpanel := THtPanel.Create(FMainform);
  Fhtpanel.Parent := FMainform;

  {$IFDEF FPC}
  Fhtpanel.Align := alclient;
  {$ENDIF}
  {$IFDEF DCC}
  Fhtpanel.Align := TAlignLayout.client;
  {$ENDIF}
  FMainform.show;
end;

destructor TCrossForm.Destroy();
begin
  FMainform.Free;
  inherited;
end;

procedure TCrossForm.LoadHTMLFromFile(const FileName: string; AOnloadEvent : TProcEvent);
begin
  Fhtpanel.LoadFromFile(FileName);
  AOnloadEvent();
end;

procedure TCrossForm.LoadHTMLFromURL(const URL: string ;AOnloadEvent : TProcEvent);
begin
  Fhtpanel.LoadFromURL(URL);
  AOnloadEvent();
end;

function TCrossForm.getDOM : ICrossElement;
begin
  Result := TCrossElement.Create(Fhtpanel.Document);
end;

{ THtCrossadapter }

procedure THtCrossadapter.Run(Element: TElement; const Event: string; const Script: hstring);
var
  LProc : TProcEvent;
begin
  if Element.Id = '' then Exit;

  if gRegisteredEvents.TryGetValue(Element.Id+Event, LProc) then
    LProc();
end;


initialization
  HtGlobal.ScriptAdapterClass := THtCrossadapter;
  gRegisteredEvents := TDictionary<string, TProcEvent>.Create;

finalization
  gRegisteredEvents.Free;


end.
