unit CrossMonkey;

interface


type

  TProcEvent = procedure of object;

  // ICrossElement
  //
  {:@desc A cross html element
  }
  ICrossElement = interface
  ['{d154d1bd-b6d3-4e92-9eab-392cbe1db267}']
    function getElementById(AId : string) : ICrossElement;
    function childrenCount : Integer;
    function getChildren(AIndex : Integer) : ICrossElement;
    procedure setAttribute(AName, AValue : string);
    function getAttribute(AName : string) : string;
    procedure setStylePropertie(AName, AValue : string);
    function getStylePropertie(AName : string) : string;
    function getInnertText : string;
    procedure setInnerText(AInnerText : string);
    function getInnerHTML : string;
    procedure setInnerHTML(AInnerText : string);
    procedure focus;
    procedure addEventListener(AEvent : string; AProcedure : TProcEvent; AUseCapture : boolean);
    procedure RemoveEventListener(AEvent : string; AProcedure : TProcEvent);

    //input element
    //
    function getValue : string;
    procedure setValue(AValue : string);

  end;


  // ICrossForm
  //
  {:@desc A cross html form
  }
  ICrossForm = interface
  ['{ac627fd7-3b4b-42a0-b9f8-e5b8ef29fe54}']
    procedure LoadHTMLFromFile(const FileName: string; AOnloadEvent : TProcEvent);
    procedure LoadHTMLFromURL(const URL : string; AOnloadEvent : TProcEvent);
    function getDOM : ICrossElement;
  end;


  // ICrossApplication
  //
  {:@desc A cross application
  }
  ICrossApplication = interface
  ['{81f87619-4828-43a2-9729-f8150a9e8339}']
    {:@desc run app }
    procedure Run;
    {:@desc create a new form}
    function CreateForm : ICrossForm;
    {:@desc terminate the app}
    procedure Terminate;
  end;



implementation

end.
