#build for windows 
echo "Crossbuild" /src/$1

#find . -name pas2js.cfg
#find . -name fpc.cfg

echo "build win32"
mkdir /src/win32
/root/development/fpc/bin/x86_64-linux/fpc @/root/development/fpc/bin/x86_64-linux/fpc.cfg -Pi386 -Twin32 -va /src/$1 -FE/src/win32

echo "build win64"
mkdir /src/win64
/root/development/fpc/bin/x86_64-linux/fpc @/root/development/fpc/bin/x86_64-linux/fpc.cfg -Twin64 -va /src/$1 -FE/src/win64

echo "build linux64"
mkdir /src/linux-x64
/root/development/fpc/bin/x86_64-linux/fpc @/root/development/fpc/bin/x86_64-linux/fpc.cfg -Tlinux -va /src/$1 -FE/src/linux-x64

echo "build browser"
mkdir /src/browser
/root/development/fpc/bin/x86_64-linux/pas2js @/root/development/ccr/pas2js-rtl/bin/x86_64-linux/pas2js.cfg -Jc -Jirtl.js -MDelphi -va /src/$1 -FE/src/browser

#echo "build osx64"
#mkdir /src/osx64
#/root/development/fpc/bin/x86_64-linux/fpc @/root/development/fpc/bin/x86_64-linux/fpc.cfg -Tdarwin -va /src/$1 -FE/src/osx64

