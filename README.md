# **CrossMonkey**

**Build native desktop, mobile and web app with same UI and same codebase.
Built for pascal developpers**

<p align="middle"><img src="img/browser.png" alt="browser app" height="250" align="middle">
<img src="img/desktop.png" alt="browser app" height="220" align="middle" margin-left: 20px>
<img src="img/mobile.jpg" alt="browser app" height="220" align="middle" margin-left: 20px"></p>

Crossmonkey is an **EXPERIMENTAL** application framework that work with Delphi, Freepascal and Pastojs.

## Features:
- Design modern UI with html and css
- The same API as in your browser. You have access to a virtual dom.
- Compile to true native desktop or mobile app (Delphi or FPC)
- Compile to javascript with pas2js
- Use the same code
- For desktop and mobile, use a 100% pure pascal html renderer (the excellent delphi html component)
- For web, use direct native rendering of the browser
- Very simple

## How it works
We have declared a set of interfaces ICrossApplication, ICrossForm, ICrossElement in CrossMonkey.pas. Each ICrossForm have a DOM.
Today we have two implementations which are in fact wrappers: CrossMonkeyHTMLcomponentsImpl.pas and CrossMonkeyPAs2jsImpl.pas.

The CrossMonkeyHTMLcomponentsimpl.pas can be use to compile with delphi or fpc for native desktop or mobile app.
HTML Component libray provide several rendering canvases (GDI, GDI+, Direct2D, FMX, Android, iOS, OSX, Linux, Lazarus ...)
>"HTML Component Library is a cross-patform and 100% native HTML rendering library for Delphi and Lazarus that brings all the power of HTML/CSS into desktop and mobile Delphi applications.
> Unlike other «HTML-like» libraries it is based on powerful HTML/CSS rendering core with full support of HTML 4.1 and CSS 3 (some of HTML 5 tags are also supported). Tables, shadows, transitions, animations, SVG images, tranforms and much more.
> No DLL’s, no ActiveX, no third party libraries dependencies, only pure native Delphi code."
>https://www.delphihtmlcomponents.com/

The CrossMonkeyPAs2jsImpl.pas is use to compile to javascript.
The support of html and css is perfect because it's rendered by the browser.
This implementation wraps directly the dom api of the browser.



## Getting started

Example of crossMonkey app:

the main program

```pascal
program simple;
{$IFDEF FPC}
    {$MODE Delphi}
{$ENDIF}
uses
  CrossMonkey,
  {$ifdef pas2js}
  CrossMonkeyPas2JsImpl,
  {$else}
  CrossMonkeyHTMLComponentsImpl,
  {$endif}
  LoginController;

var
  LCrossApplication : ICrossApplication;
  LForm : ICrossForm;
  LLoginController : TLoginController;
begin
  LCrossApplication := TCrossApplication.Create;
  LForm := LCrossApplication.CreateForm;
  LLoginController := TLoginController.Create(LForm);
  LCrossApplication.run;
end.

```

The controller

```pascal
unit LoginController;

interface

uses
  Classes,
  SysUtils,
  CrossMonkey;


type

  { TLoginController }

  TLoginController = class(TObject)
    private
      FLoginForm : ICrossForm;
    public
      constructor Create(ALoginForm : ICrossForm);
      procedure OnLoad;
      procedure OnClickLogin;
  end;
  
implementation

{ TLoginController }

constructor TLoginController.Create(ALoginForm: ICrossForm);
begin
  FLoginForm := ALoginForm;
  FLoginForm.LoadHTMLFromFile('form.html', OnLoad);
end;

procedure TLoginController.OnLoad;
begin
  FLoginForm.getDOM().getElementById('submit')
    .addEventListener('click', OnClickLogin , True);
end;

procedure TLoginController.OnClickLogin;
var
    LLogin, LPassword : string;
begin
  LLogin := FLoginForm.getDOM.getElementById('login').getValue;
  LPassword := FLoginForm.getDOM.getElementById('password').getValue;

  FLoginForm.getDOM.getElementById('message').
    setInnerHTML('<p>Log with user login:' + LLogin  + ' and password:' + LPassword+'<p>');
end;

end.
```

## To do
There are lot to do !
We are only to the **proof of concept** stage.
Any help will be appreciated


- Enrich dom functions (today, it's only a very small subset) and not only for the UI https://developer.mozilla.org/fr/docs/Web/API
    - More dom manipulation functions
    - Network capcity (web socket etc..)
    - Geolocation
    - Audio
    - etc..

- Add a CEF Chronium implementation (Add a big dependency but can bring high performance and perfect rendering for desktop app). But it will be still a native app, not a javascript app.
- A web assembly implementation when official support will be available with PFC
- Tests

...

