program simple;
{$IFDEF FPC}
    {$MODE Delphi}
{$ENDIF}
uses
  CrossMonkey,
  {$ifdef pas2js}
  CrossMonkeyPas2JsImpl,
  {$else}
  CrossMonkeyHTMLComponentsImpl,
  {$endif}
  LoginController;

var
  LCrossApplication : ICrossApplication;
  LForm : ICrossForm;
  LLoginController : TLoginController;
begin
  LCrossApplication := TCrossApplication.Create;
  LForm := LCrossApplication.CreateForm;
  LLoginController := TLoginController.Create(LForm);
  LCrossApplication.run;
end.
