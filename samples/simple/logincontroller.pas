unit LoginController;

{$IFDEF FPC}
  {$MODE Delphi}
{$ENDIF}
  {$ifdef pas2js}
  {$MODE Delphi}
{$ENDIF}
interface

uses
  Classes,
  SysUtils,
  CrossMonkey;


type

  { TLoginController }

  TLoginController = class(TObject)
    private
      FLoginForm : ICrossForm;
    public
      constructor Create(ALoginForm : ICrossForm);
      procedure OnLoad;
      procedure OnClickLogin;
  end;

implementation

{ TLoginController }

constructor TLoginController.Create(ALoginForm: ICrossForm);
begin
  FLoginForm := ALoginForm;
  FLoginForm.LoadHTMLFromFile('form.html', {$ifdef pas2js}@{$endif}OnLoad);
end;

procedure TLoginController.OnLoad;
begin
  FLoginForm.getDOM().getElementById('submit')
    .addEventListener('click', {$ifdef pas2js}@{$endif}OnClickLogin , True);
end;

procedure TLoginController.OnClickLogin;
var
    LLogin, LPassword : string;
begin
  LLogin := FLoginForm.getDOM.getElementById('login').getValue;
  LPassword := FLoginForm.getDOM.getElementById('password').getValue;

  FLoginForm.getDOM.getElementById('message').
    setInnerHTML('<p>Log with user login:' + LLogin  + ' and password:' + LPassword+'<p>');
end;

end.

